#!/usr/bin/env bash
#
# set -x;
set -e;

edexFile=${1};
edexType=${2};
xmllint --noout --schema ./Edexml-${edexType}/EDEXML.structuur.xsd ${edexFile};

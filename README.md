**How-to validate**

Install xmllint (see: http://xmlsoft.org/downloads.html)

- on mac use ```brew install libxml2```
- on linux use ```sudo apt-get install libxml2-utils```
- on windows ...

Validate against edexml version 1.0.3 or 2.0

Run: ```./validate.sh Edexfile.xml 2.0```